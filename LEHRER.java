
/**
 * Beschreiben Sie hier die Klasse LEHRER.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */
public class LEHRER extends CONTENDER
{
        /**
     * Konstruktor für Objekte der Klasse LEHRER
     */
    public LEHRER()
    {
       super(); 
    }

    public double gibWkeitSchere()
    {
        return 1.0;
    }
    
    public double gibWkeitStein()
    {
        return 0.0;
    }
    
    public double gibWkeitPapier()
    {
        return 0.0;
    }
   
}
